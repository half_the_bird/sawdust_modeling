package com.pb.sawdust.model.models.provider.hub;

/**
 * The {@code HubPackageTests} ...
 *
 * @author crf <br/>
 *         Started Oct 5, 2010 12:33:34 AM
 */
public class HubPackageTests {
    public static void main(String ... args) {
        SimpleDataProviderHubTest.main();
        SubDataProviderHubTest.main();
        SimplePolyDataProviderTest.main();
        SubPolyDataProviderTest.main();

        SimpleCalculationPolyDataProviderTest.main();
        CachedCalculationPolyDataProviderTest.main();
        FullCalculationPolyDataProviderTest.main();
    }
}
