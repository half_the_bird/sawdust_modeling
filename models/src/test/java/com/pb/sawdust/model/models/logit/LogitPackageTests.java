package com.pb.sawdust.model.models.logit;

import com.pb.sawdust.model.models.logit.LogitModelTest;

/**
 * The {@code LogitPackageTests} ...
 *
 * @author crf <br/>
 *         Started Sep 28, 2010 2:36:06 PM
 */
public class LogitPackageTests {
    public static void main(String ... args) {
        LogitModelTest.main();
        NestedLogitUtilityTest.main();
        NestedLogitModelTest.main();
        PolyLogitModelTest.main();
    }
}
