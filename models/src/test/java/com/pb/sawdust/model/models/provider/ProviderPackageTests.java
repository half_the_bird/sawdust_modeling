package com.pb.sawdust.model.models.provider;

import com.pb.sawdust.model.models.provider.hub.HubPackageTests;

/**
 * The {@code ProviderPackageTests} ...
 *
 * @author crf <br/>
 *         Started Sep 25, 2010 9:30:00 AM
 */
public class ProviderPackageTests {
    public static void main(String ... args) {
        AbstractIdDataTest.main();
        ExpandableDataProviderTest.main();
        SimpleDataProviderTest.main();
        DataTableDataProviderTest.main();
        LazySubDataProviderTest.main();
        CompositeDataProviderTest.main();
        UtilityDataProviderTest.main();
        SimpleCalculationDataProviderTest.main();
        FullCalculationDataProviderTest.main();
        CachedCalculationDataProviderTest.main();
        HubPackageTests.main();
    }
}
