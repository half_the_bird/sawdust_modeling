package com.pb.sawdust.model.models.utility;

/**
 * The {@code UtilityPackageTests} ...
 *
 * @author crf <br/>
 *         Started Sep 17, 2010 12:46:47 AM
 */
public class UtilityPackageTests {
    public static void main(String ... args) {
        SimpleLinearUtilityTest.main();
        EmptyUtilityTest.main();
        LinearCompositeUtilityTest.main();
    }
}
