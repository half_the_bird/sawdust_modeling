package com.pb.sawdust.model.models.provider;

/**
 * The {@code IdData} interface is used to associate unique identifiers with sources of data.  The identifiers are
 * intended to be used for caching purposes so that users of the data do not have to perform redundant calculations.
 * Because of this, it is important that identifiers for different data sources are distinct (at least contextually).
 *
 * @author crf <br/>
 *         Started Sep 3, 2010 10:06:03 AM
 */
public interface IdData {
    /**
     * Get the id for the data.
     *
     * @return the data id.
     */
    int getDataId();
}
