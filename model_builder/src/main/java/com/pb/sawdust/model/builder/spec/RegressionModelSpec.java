package com.pb.sawdust.model.builder.spec;

/**
 * The {@code RegressionModelSpec} ...
 *
 * @author crf <br/>
 *         Started 4/12/11 10:55 AM
 */
public interface RegressionModelSpec {
    LinearUtilitySpec getUtilitySpec();
}
