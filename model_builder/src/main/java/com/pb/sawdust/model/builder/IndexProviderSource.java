package com.pb.sawdust.model.builder;

import com.pb.sawdust.model.models.provider.tensor.IndexProvider;

/**
 * The {@code IndexProviderSource} ...
 *
 * @author crf
 *         Started 6/5/12 9:58 AM
 */
public interface IndexProviderSource {
    IndexProvider getIndexProvider();
}
