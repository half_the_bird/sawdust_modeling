package com.pb.sawdust.model.builder.spec;

import java.util.Map;

/**
 * The {@code UtilitySpec} ...
 *
 * @author crf <br/>
 *         Started 4/12/11 11:00 AM
 */
public interface LinearUtilitySpec {
    String getName();
    Map<VariableSpec,CoefficientSpec> getUtilitySpecMap();
}
