package com.pb.sawdust.model.builder;

import com.pb.sawdust.model.models.provider.DataProvider;

/**
 * The {@code DataProviderBuilder} ...
 *
 * @author crf <br/>
 *         Started 4/12/11 11:51 AM
 */
public interface DataProviderBuilder {
    DataProvider getProvider();
}
