package com.pb.sawdust.model.builder;

import com.pb.sawdust.tabledata.DataTable;

/**
 * The {@code DataTableSource} ...
 *
 * @author crf
 *         Started 5/29/12 11:00 AM
 */
public interface DataTableSource {
    DataTable getDataTable();
}
