package com.pb.sawdust.model.builder.parser;

import com.pb.sawdust.calculator.ParameterizedNumericFunction;

/**
 * The {@code FunctionBuilder} ...
 *
 * @author crf <br/>
 *         Started 4/17/11 5:56 AM
 */
public interface FunctionBuilder {

    ParameterizedNumericFunction buildFunction(String formula);
}
