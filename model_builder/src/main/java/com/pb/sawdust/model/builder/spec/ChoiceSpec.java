package com.pb.sawdust.model.builder.spec;

import com.pb.sawdust.model.models.Choice;

/**
 * The {@code ChoiceSpec} ...
 *
 * @author crf
 *         Started 4/30/12 6:14 AM
 */
public interface ChoiceSpec {
    String getName();
}
