package com.pb.sawdust.model.builder.spec;

/**
 * The {@code ModelTypeSpec} ...
 *
 * @author crf <br/>
 *         Started 4/11/11 4:28 PM
 */
public interface ModelTypeSpec {
    String getTypeName();
    String getTypeVersion();
}
