package com.pb.sawdust.model.builder;

import com.pb.sawdust.model.builder.spec.LinearUtilitySpec;
import com.pb.sawdust.model.models.utility.LinearUtility;

/**
 * The {@code LinearUtilityBuilder} ...
 *
 * @author crf <br/>
 *         Started 4/12/11 11:05 AM
 */
public interface LinearUtilityBuilder {
    LinearUtility buildUtility(LinearUtilitySpec utilitySpec, String coefficientBranch);
}
