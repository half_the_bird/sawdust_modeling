package com.pb.sawdust.model.builder.spec;

/**
 * The {@code VariableSpec} ...
 *
 * @author crf <br/>
 *         Started 4/10/11 2:35 PM
 */
public interface VariableSpec {
    String getName();
    FormulaSpec getFormula();
}
