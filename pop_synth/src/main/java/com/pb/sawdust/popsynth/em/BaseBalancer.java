package com.pb.sawdust.popsynth.em;

import com.pb.sawdust.tabledata.DataRow;

import java.util.*;

/**
 * The {@code BaseBalancer} is a basic implementation of the {@code Balancer} interface. It is very simple and just updates
 * the elements and dimensions in the iteration order (implicitly) defined by the input (at construction) arguments.
 *
 * @author crf
 *         Started 9/30/11 6:22 AM
 */
public class BaseBalancer implements Balancer {
    final List<String> dimensionNames;
    final Map<String,BalanceDimension<?>> balanceDimensions;
    final Map<String,Map<?,Double>> targets;
    final ConvergenceInformation convergenceInformation;
    final double weightLimitFactor;

    /**
     * Constructor specifying the name and necessary specifications for the balancer.
     *
     * @param associationName
     *        The name of the balancer. This is used in reporting (convergence information).
     *
     * @param criteria
     *        The convergence criteria. This is in the form of a map from the balance dimension classifier to its associated
     *        convergence criterion value. The iterator order of the keys defines the balancing order.
     *
     * @param maximumIterations
     *        The maximum allowed balance iterations.
     *
     * @param elements
     *        The balance elements that the balancer will act upon. The iteration order defines the order in which the balancing
     *        will take place.
     *
     * @param targetData
     *        The target data. The necessary target values must be buildable from the keys of {@code criterion}.
     *
     * @param weightLimitFactor
     *        A factor used to limit the size of the weights for each balance element. Used by {@code BalanceDimension}
     *        when updating weights.
     *
     * @see BalanceDimension#updateWeights(java.util.Map,double)
     */
    @SuppressWarnings("unchecked") //ok because we are transferring all unknown type arguments to ?
    public BaseBalancer(String associationName, Map<BalanceDimensionClassifier,Double> criteria, int maximumIterations, Iterable<? extends BalanceElement> elements, DataRow targetData, double weightLimitFactor) {
        balanceDimensions = new HashMap<>();
        targets = new HashMap<>();
        dimensionNames = new LinkedList<>();

        Map<String,Double> convergenceCriteria = new HashMap<>();
        for (BalanceDimensionClassifier classifier : criteria.keySet()) {
            String name = classifier.getDimensionName();
            dimensionNames.add(name);
            balanceDimensions.put(name,new BalanceDimension(classifier,elements));
            targets.put(name,buildTargets(classifier,targetData));
            convergenceCriteria.put(name,criteria.get(classifier));
        }
        this.weightLimitFactor = weightLimitFactor;
        convergenceInformation = buildConvergenceInformation(associationName,targets,convergenceCriteria,maximumIterations);
    }

    /**
     * Convenience (sort of) constructor which uses a single convergence criterion value for all dimensions.
     *
     * @param associationName
     *        The name of the balancer. This is used in reporting (convergence information).
     *
     * @param classifiers
     *        The balance dimension classifiers used during balancing. The iteration order of this collection defines the
     *        balancing order.
     *
     * @param criterion
     *        The convergence criterion. This value will be used across all dimensions.
     *
     * @param maximumIterations
     *        The maximum allowed balance iterations.
     *
     * @param elements
     *        The balance elements that the balancer will act upon. The iteration order defines the order in which the balancing
     *        will take place.
     *
     * @param targetData
     *        The target data. The necessary target values must be buildable from the keys of {@code criterion}.
     *
     * @param weightLimitFactor
     *        A factor used to limit the size of the weights for each balance element. Used by {@code BalanceDimension}
     *        when updating weights.
     *
     * @see BalanceDimension#updateWeights(java.util.Map,double)
     */
    public BaseBalancer(String associationName, Collection<BalanceDimensionClassifier> classifiers, double criterion, int maximumIterations, Iterable<? extends BalanceElement> elements, DataRow targetData, double weightLimitFactor) {
        this(associationName,buildCriteria(classifiers,criterion),maximumIterations,elements,targetData,weightLimitFactor);
    }

    ConvergenceInformation buildConvergenceInformation(String associationName, Map<String,Map<?,Double>> targets, Map<String,Double> convergenceCriterion, int maximumIterations) {
        return new BaseConvergenceInformation(associationName,targets,convergenceCriterion,maximumIterations);
    }

    @SuppressWarnings("unchecked") //cast to <?> is valid here, can't construct at first because it is then unmodifiable
    private static Map<BalanceDimensionClassifier,Double> buildCriteria(Collection<BalanceDimensionClassifier> classifiers, double criterion) {
        Map<BalanceDimensionClassifier,Double> m = new HashMap<>();
        for (BalanceDimensionClassifier classifier : classifiers)
            m.put(classifier,criterion);
        return m;
    }

    @SuppressWarnings("unchecked") //we don't know about a specific T, but targets will match up to balance dimensions
    public void updateWeights() {
        for (String dimensionName : dimensionNames)
            updateWeights(dimensionName);
    }

    @SuppressWarnings("unchecked") //these generics are correct and internally consistent
    void updateWeights(String dimensionName) {
        BalanceDimension bd = balanceDimensions.get(dimensionName);
        Map<?,Double> target = targets.get(dimensionName);
        bd.updateWeights(target,weightLimitFactor);
        convergenceInformation.updateDimension(dimensionName,bd.getControlTotals());
    }

    public void updateControlsAndTargets() {
        for (String dimensionName : dimensionNames)
            convergenceInformation.updateDimension(dimensionName,balanceDimensions.get(dimensionName).getControlTotals());
    }

    public void balance() {
        while (!convergenceInformation.meetsStoppingCriteria())
            updateWeights();
    }

    public ConvergenceInformation getConvergenceInformation() {
        return convergenceInformation;
    }

    private <T> Map<T,Double> buildTargets(BalanceDimensionClassifier<T> classifier, DataRow targetData) {
        return new LinkedHashMap<>(classifier.getTargetMap(targetData)); //to preserve whatever ordering might be present in elements
    }

}
