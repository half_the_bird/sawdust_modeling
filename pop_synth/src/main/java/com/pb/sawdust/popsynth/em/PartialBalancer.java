package com.pb.sawdust.popsynth.em;

import com.pb.sawdust.tabledata.DataRow;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * The {@code PartialBalancer} is a {@code Balancer} which only balances elements in a selected number of dimensions. This
 * can be useful when a full balance is not possible (for logical reasons), but a full reporting of all dimensions (even
 * those not balanced) is desired.
 *
 * @author crf
 *         Started 4/19/13 9:22 AM
 */
public class PartialBalancer extends BaseBalancer {
    private final List<String> activeDimensions;

    /**
     * Constructor specifying the name and necessary specifications for the balancer, including which dimensions to balance
     * against.
     *
     * @param associationName
     *        The name of the balancer. This is used in reporting (convergence information).
     *
     * @param criteria
     *        The convergence criteria. This is in the form of a map from the balance dimension classifier to its associated
     *        convergence criterion value. The iterator order of the keys defines the balancing order.
     *
     * @param maximumIterations
     *        The maximum allowed balance iterations.
     *
     * @param elements
     *        The balance elements that the balancer will act upon. The iteration order defines the order in which the balancing
     *        will take place.
     *
     * @param targetData
     *        The target data. The necessary target values must be buildable from the keys of {@code criterion}.
     *
     * @param weightLimitFactor
     *        A factor used to limit the size of the weights for each balance element. Used by {@code BalanceDimension}
     *        when updating weights.
     *
     * @param activeDimensions
     *        The dimension names (as defined by the balance classifiers) that will be balanced against during the balancing
     *        procedure.
     *
     * @see BalanceDimension#updateWeights(java.util.Map,double)
     */
    public PartialBalancer(String associationName, Map<BalanceDimensionClassifier,Double> criteria, int maximumIterations, Iterable<? extends BalanceElement> elements, DataRow targetData, double weightLimitFactor, List<String> activeDimensions) {
        super(associationName,criteria,maximumIterations,elements,targetData,weightLimitFactor);
        if (!dimensionNames.containsAll(activeDimensions))
            throw new IllegalArgumentException("All active dimensions (" + activeDimensions.toString() + ") not found in base dimensions (" + dimensionNames + ")");
        this.activeDimensions = activeDimensions;
    }

    /**
     * Constructor specifying the name and necessary specifications for the balancer, including which dimensions to balance
     * against.
     *
     * @param associationName
     *        The name of the balancer. This is used in reporting (convergence information).
     *
     * @param classifiers
     *        The balance dimension classifiers used during balancing. The iteration order of this collection defines the
     *        balancing order.
     *
     * @param criterion
     *        The convergence criterion. This value will be used across all dimensions.
     *
     * @param maximumIterations
     *        The maximum allowed balance iterations.
     *
     * @param elements
     *        The balance elements that the balancer will act upon. The iteration order defines the order in which the balancing
     *        will take place.
     *
     * @param targetData
     *        The target data. The necessary target values must be buildable from the keys of {@code criterion}.
     *
     * @param weightLimitFactor
     *        A factor used to limit the size of the weights for each balance element. Used by {@code BalanceDimension}
     *        when updating weights.
     *
     * @param activeDimensions
     *        The dimension names (as defined by the balance classifiers) that will be balanced against during the balancing
     *        procedure.
     *
     * @see BalanceDimension#updateWeights(java.util.Map,double)
     */
    public PartialBalancer(String associationName, Collection<BalanceDimensionClassifier> classifiers, double criterion, int maximumIterations, Iterable<? extends BalanceElement> elements, DataRow targetData, double weightLimitFactor, List<String> activeDimensions) {
        super(associationName,classifiers,criterion,maximumIterations,elements,targetData,weightLimitFactor);
        if (!dimensionNames.containsAll(activeDimensions))
            throw new IllegalArgumentException("All active dimensions (" + activeDimensions.toString() + ") not found in base dimensions (" + dimensionNames + ")");
        this.activeDimensions = activeDimensions;
    }

    ConvergenceInformation buildConvergenceInformation(String associationName, Map<String,Map<?,Double>> targets, Map<String,Double> convergenceCriteria, int maximumIterations) {
        return new PartialConvergenceInformation(associationName,targets,convergenceCriteria,maximumIterations);
    }

    public void updateWeights() {
        for (String dimensionName : activeDimensions)
            updateWeights(dimensionName);
    }

    private class PartialConvergenceInformation extends BaseConvergenceInformation {
        public PartialConvergenceInformation(String associationName, Map<String,Map<?,Double>> targets, Map<String,Double> convergenceCriteria, int maxUpdates) {
            super(associationName,targets,convergenceCriteria,maxUpdates);
        }

        boolean isConverged(String dimensionName, boolean previous) {
            return !activeDimensions.contains(dimensionName) || super.isConverged(dimensionName,previous);
        }
    }
}
